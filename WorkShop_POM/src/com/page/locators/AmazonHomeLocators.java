package com.page.locators;

import org.openqa.selenium.By;

public interface AmazonHomeLocators {
	
	public static By SEARCH_FIELD = By.id("searchDropdownBox");
	public static By GO_BTN = By.xpath("//input[@type='submit']");
	public static By MEN = By.xpath("//span[text()='MEN']");
	public static By SHIRTS = By.xpath("//a[text()='Shirts']");
	
	

}
