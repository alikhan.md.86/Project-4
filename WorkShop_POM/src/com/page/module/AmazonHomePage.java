package com.page.module;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.page.locators.AmazonHomeLocators;

public class AmazonHomePage implements AmazonHomeLocators{
	
	private WebDriver driver;
	
	public AmazonHomePage(WebDriver driver){
		
		this.driver = driver;
	}
	
	
	public void searchForShoesAndHandBag(){
		
		
		WebElement dropdown = driver.findElement(SEARCH_FIELD);
		
		 Select sel = new Select(dropdown);
		 sel.selectByVisibleText("Shoes & Handbags");
		 driver.findElement(GO_BTN).click();
	}
	
	public AmazonShirtsPage navigateToShirtsPage(){
		
		WebElement mouseover_menitem = driver.findElement(MEN);
		
		 Actions mouseovermenu=new Actions(driver);
		 mouseovermenu.moveToElement(mouseover_menitem).build().perform();
		 try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 WebElement selectTShirts = driver.findElement(SHIRTS);
		 mouseovermenu.moveToElement(selectTShirts).click().build().perform();
		 	 
		 return new AmazonShirtsPage(driver);
		 
	}
	
	

}
