package com.page.module;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.page.locators.AmazonHomeLocators;
import com.page.locators.AmazonShirtsLocators;

public class AmazonShirtsPage implements AmazonShirtsLocators{
	
	private WebDriver driver;
	
	public AmazonShirtsPage(WebDriver driver){
		
		this.driver = driver;
	}
	
	public void clickOnItem(){
		
		driver.findElement(ITEM).click();
	}
	
	
	

}
