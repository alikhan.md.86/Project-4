package com.testsuite;

import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.page.module.AmazonHomePage;
import com.page.module.AmazonShirtsPage;


public class TestAddItemToCart {
	
	private WebDriver driver;
	
	AmazonHomePage homePage;
	AmazonShirtsPage shirtsPage;
	
	
	@BeforeMethod
	public void doSetUp(){
		
		String path = System.getProperty("user.dir")+"\\Resources\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", path);
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://www.amazon.in/");
		
	}
	
	@Test
	public void testAddItemToCart(){
		
		homePage = new AmazonHomePage(driver);
		homePage.searchForShoesAndHandBag();
		shirtsPage = homePage.navigateToShirtsPage();
		shirtsPage.clickOnItem();
		
		
		
		
	}
	
	@AfterMethod
	public void undoSetUp(){
		
	//	driver.quit();
	}

}
