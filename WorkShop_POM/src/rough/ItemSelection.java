package rough;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class ItemSelection {

	public static void main(String[] args) throws InterruptedException {
		
	//	FirefoxDriver driver = new FirefoxDriver();
		
		String path = System.getProperty("user.dir")+"\\Resources\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", path);
		ChromeDriver driver= new ChromeDriver();
		driver.get("http://www.amazon.in/");
		driver.findElement(By.xpath(".//*[@id='searchDropdownBox']")).click();
		WebElement dropdown = driver.findElement(By.xpath(".//*[@id='searchDropdownBox']"));
		
		 Select sel = new Select(dropdown);
		 sel.selectByVisibleText("Shoes & Handbags");
		 
		 driver.findElement(By.xpath("//input[@type='submit']")).click();
		 WebElement mouseover_menitem = driver.findElement(By.xpath("//span[text()='MEN']"));
		
		 Actions mouseovermenu=new Actions(driver);
		 mouseovermenu.moveToElement(mouseover_menitem).build().perform();
		 Thread.sleep(5000);
		 WebElement selectTShirts = driver.findElement(By.xpath("//a[text()='Shirts']"));
		 mouseovermenu.moveToElement(selectTShirts).click().build().perform();
		 
		// driver.findElement(By.xpath("//label[contains(.,'VERO LIE')]/input[@type='checkbox']")).click();
		 
		 driver.findElement(By.xpath("//*[@id='result_0']/div/div[2]/div[1]/a/h2")).click();
		 
		 Set<String> windowSize=driver.getWindowHandles();
		 
		 
		 System.out.println("win "+ windowSize.size());
		 Iterator<String> itr = windowSize.iterator();
		 itr.next();
		 driver.switchTo().window(itr.next());
		 Thread.sleep(10000);
		 WebElement sizeDropDown = driver.findElement(By.id("native_dropdown_selected_size_name"));
		 Select sizedropdown = new Select(sizeDropDown);
		 sizedropdown.selectByIndex(1);
		 Thread.sleep(2000);
		 driver.findElement(By.xpath("//input[@value='Add to Cart']")).click();

	}

}
